# 自动浇花套件V2.0  


![](./arduinoC/_images/featured.png)
  
  ## 目录

* [相关链接](#相关链接)
* [描述](#描述)
* [积木列表](#积木列表)
* [示例程序](#示例程序)
* [许可证](#许可证)
* [支持列表](#支持列表)
* [更新记录](#更新记录)  

## 相关链接  

* 本项目加载链接```https://gitee.com/jeff111203/ext.zdjh-master```   
* 用户库教程链接: ```https://mindplus.dfrobot.com.cn/extensions-user```   
* 购买此产品: [商城](https://www.dfrobot.com.cn/goods-503.html).  
## 描述  

基于Arduino Leonardo控制器设计，使用土壤湿度传感器，配上空气温湿度传感器，可以实时监测当前温湿度和土壤湿度。可手动设置湿度阈值，当湿度低于一定数值时，自动开启水泵。适用于不同品类的植物。板载一个Xbee接口，可插WiFi,蓝牙，Zigbee等无线设备，连接到本地网关，使实现物联网远程控制。即使不在家也可以自动照顾您的花草，免除您的后顾之忧。  

## 积木列表  

![](./arduinoC/_images/blocks.png)  

## 示例程序
  
**测量温湿度及土壤湿度：**    

![](./arduinoC/_images/example1.png) 

**控制水泵：**  
  
![](./arduinoC/_images/example2.png)  

## 许可证  

MIT  

## 支持主板  


主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
leonardo      |             |        √      |             |   
  
    
## 更新日志

* V0.0.1  基础功能完成
* V0.0.2  根据Mind+V1.6.2 RC2.0更新此库