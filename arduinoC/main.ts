
//% color="#AA278D" iconWidth=50 iconHeight=40
 namespace ZDJH{
     //% block="Auto watering read pin[PIN] dht11[TH] " blockType="reporter"
     //% PIN.shadow="dropdownRound"   PIN.options="PIN"
     //% TH.shadow="dropdownRound"   TH.options="TH"
     export function ZDJHInitT(parameter: any, block: any) {
         let pin=parameter.PIN.code;
         let th=parameter.TH.code;
         Generator.addInclude('ZDJHInitinclude', '#include <DFRobot_DHT.h>');
         Generator.addObject("ZDJHInitobjectdht","DFRobot_DHT",`dht11_${pin};`);
         Generator.addSetup("ZDJHInitinclude11",`dht11_${pin}.begin(${pin},DHT11);`,true);
         Generator.addCode([`dht11_${pin}.get${th}()`,Generator.ORDER_UNARY_POSTFIX]);
         
     }
     //% block="Auto watering read soil[SOIL] " blockType="reporter"
     //% SOIL.shadow="dropdownRound"   SOIL.options="SOIL"
     export function ZDJHInitS(parameter: any, block: any) {
         let soil=parameter.SOIL.code;
        Generator.addCode([`analogRead(${soil})`,Generator.ORDER_UNARY_POSTFIX]);

     }


     //% block="Auto watering is Water pump pin(5,6) key[KEY]" blockType="command"
     //% KEY.shadow="dropdown" KEY.options="KEY"
     export function ZDJHInitK(parameter: any, block: any) {
         let key=parameter.KEY.code;
         Generator.addSetup("ZDJHInitSetup1",`pinMode(5, OUTPUT);`);
         Generator.addSetup("ZDJHInitSetup2",`pinMode(6, OUTPUT);`);
         Generator.addCode(`digitalWrite(5, ${key});`);
         Generator.addCode(`digitalWrite(6, ${key});`);
     }

    
}
